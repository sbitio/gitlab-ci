# GitLab CI

Templates, Examples, Docker Images & Tools for GitLab CI

## Goals
- [x] Develop reusable gitlab-ci templates&tools
- [x] Fire jenkins jobs from gitlab-ci retriving output (no more fire&forget)
- [ ] Bring jenkins artifacts to gitlab for integrated MR reporting
- [ ] Develop tests for all this wiring

## Content

### Docker images

- `registry.gitlab.com/sbitio/gitlab-ci:latest`: Image providing tools

### Library

- `sbit.io-Jenkins.gitlab-ci.yml`:
  - Job model `.sbitio_jenkins_job_trigger`, requires `${JENKINS_USER}`, `${JENKINS_TOKEN}`, `${JENKINS_HOST}` & `${JENKINS_JOB}` variables
- `sbit.io-Docker.gitlab-ci.yml`:
  - Job model `.sbitio_dind_jobs`, for cached docker in docker
  - Job model `.sbitio_docker_build`, for docker builds


### Examples

- `examples/curl.gitlab-ci.yml`: Job using curl image
- `examples/docker-dir.gitlab-ci.yml`: Jobs to build and security test a docker container located on a directory
- `examples/pass-env-vars.gitlab-ci.yml`: Jobs passing environment variables

## Development
Development happens on Gitlab.
Please log issues for any bug report, feature or support request.

## License
MIT License, see LICENSE file

## Contact
Please use the contact form on http://sbit.io
