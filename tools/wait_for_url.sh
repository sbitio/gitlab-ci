#!/bin/bash

if [ "${TRACE}" == "true" ]; then
  set -x
  CURL_TRACE="-v"
fi

: ${TIMEOUT:=900}
: ${SUCCESS_CODE:=200}

echo "Testing $1"
timeout -s TERM $TIMEOUT bash -c \
  'while [[ "$(curl ${CURL_TRACE} -s -o /dev/null -L -w ''%{http_code}'' ${0})" != "200" ]]; do \
    echo "Waiting for ${0}" && sleep 2;\
  done' ${1}
echo "OK!"
curl -s ${CURL_TRACE} -I $1
