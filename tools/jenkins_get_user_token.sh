#!/bin/bash

if [ "${TRACE}" == "true" ]; then
  set -x
  CURL_TRACE="-v"
fi

usage()
{
    cat << USAGE >&2
Usage:
    -h HOST     | --host=HOST                       Jenkins host
USAGE
    exit 1
}

# process arguments
while [[ $# -gt 0 ]]
do
    case "$1" in
        -h)
        HOST="$2"
        if [[ $HOST == "" ]]; then break; fi
        shift 2
        ;;
        --host=*)
        HOST="${1#*=}"
        shift 1
        ;;
        --)
        shift
        CLI="$@"
        break
        ;;
        --help)
        usage
        ;;
        *)
        error "Unknown argument: $1"
        usage
        ;;
    esac
done

CURL="curl -s -u ${JENKINS_USER}:${JENKINS_PASS} ${CURL_TRACE}"

COOKIE_JAR="/tmp/cookies$$"
CRUMB=$(${CURL} --cookie-jar ${COOKIE_JAR} "${HOST}/crumbIssuer/api/xml?xpath=concat(//crumbRequestField,\":\",//crumb)")
CURL="${CURL} -H ${CRUMB} --cookie ${COOKIE_JAR}"

${CURL} "$HOST/me/descriptorByName/jenkins.security.ApiTokenProperty/generateNewToken" --data 'newTokenName=test' \
| jq .data.tokenValue \
| sed 's/""//g'
